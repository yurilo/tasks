# Google tasks frontend

Swing desktop GUI for Google Tasks management.
Gives user the ability to add/remove/modify Google tasks on desktop environment.

##Build & run:
 mvn clean assembly:assembly;
 cd target;
 java -jar tasks-1.0-SNAPSHOT-jar-with-dependencies.jar;

##List of TODO and FIXME tasks

####High:

- Create user preferences with ability to change shortcuts / locale / timezone / google account / notification type / syncronization interval

- Properly handle Service unavailable 503 error code from Google service: reattempt service call

- add logging for Google service calls

- Add view that shows tasks that are broken down by dates

####Major:

- Search box with autocompletion

- Use tabs to represent task lists

- Implement reusable validation control

- Application Automatic updates using JNLP(aka JWS)

- Notification popup for Due Date tasks

- Syncronization by Timer thread

- Open link in Google Calendar for Due Date tasks

####Minor:

- Add multi-monitor support

- TimeZone and Locale support

- completed tasks must be grayed

- i18n messages

- image cleanup & styles adjustments

- add Tip Labels where necessary
