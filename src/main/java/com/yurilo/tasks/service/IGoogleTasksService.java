package com.yurilo.tasks.service;

import java.util.Collection;

import com.google.inject.ImplementedBy;
import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.domain.CTaskList;
import com.yurilo.tasks.exceptions.CSystemException;

@ImplementedBy(GoogleTasksService.class)
public interface IGoogleTasksService {

	String preAuthenticate();

	boolean isAuthorized(String userId) throws CSystemException;

	void authenticate(String userId, String token) throws CSystemException;

	Collection<CTaskList> getTaskLists() throws CSystemException;

	Collection<CTask> getTasks(String listId) throws CSystemException;

	CTask insertTask(String listId, CTask cTask) throws CSystemException;

	CTask updateTask(String listId, CTask cTask) throws CSystemException;

	void deleteTask(String listId, CTask cTask) throws CSystemException;

	void deleteList(String listId) throws CSystemException;

	void deleteCompleted(String listId) throws CSystemException;

	CTaskList insertTaskList(CTaskList cTaskList) throws CSystemException;
}