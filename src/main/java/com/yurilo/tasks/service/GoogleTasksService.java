package com.yurilo.tasks.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialStore;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.services.GoogleKeyInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.tasks.Tasks;
import com.google.api.services.tasks.Tasks.TasksOperations.Clear;
import com.google.api.services.tasks.Tasks.TasksOperations.Delete;
import com.google.api.services.tasks.Tasks.TasksOperations.Insert;
import com.google.api.services.tasks.Tasks.TasksOperations.Update;
import com.google.api.services.tasks.model.Task;
import com.google.api.services.tasks.model.TaskList;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.inject.Singleton;
import com.yurilo.tasks.auth.PreferencesCredentialStore;
import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.domain.CTaskList;
import com.yurilo.tasks.exceptions.CSystemException;
import com.yurilo.tasks.util.TasksUtils;

@Singleton public class GoogleTasksService implements IGoogleTasksService {
	private static String YURILO_TASKS_SAMPLE_1_0;
	private static String REDIRECT_URL;
	private static String CLIENT_ID;
	private static String CLIENT_SECRET;
	private static String KEY;
	private static List<String> SCOPES;
	
	private GoogleAuthorizationCodeFlow flow;
	private HttpTransport transport = new NetHttpTransport();
	private JsonFactory jsonFactory = new JacksonFactory();
	private Credential loadCredential;
	private Tasks service;

	//TODO: inject it.
	static {
		YURILO_TASKS_SAMPLE_1_0 = TasksUtils.getProperties().getProperty("application.google.name");
		REDIRECT_URL = TasksUtils.getProperties().getProperty("application.google.oauth.redirect.url");
		CLIENT_ID = TasksUtils.getProperties().getProperty("application.google.client.id");
		CLIENT_SECRET = TasksUtils.getProperties().getProperty("application.google.secret");
		SCOPES = Arrays.asList(TasksUtils.getProperties().getProperty("application.google.tasks.scope.read"), TasksUtils.getProperties().getProperty("application.google.tasks.scope.write"));
		KEY = TasksUtils.getProperties().getProperty("application.google.key");
	}
	
	public GoogleTasksService() {
		//TODO: inject it.
		CredentialStore credentialStore = new PreferencesCredentialStore();
		flow = new GoogleAuthorizationCodeFlow.Builder(transport, jsonFactory, CLIENT_ID, CLIENT_SECRET, SCOPES)
		.setCredentialStore(credentialStore).build();
	}

	/** {@inheritDoc} */
	@Override public String preAuthenticate() {
		GoogleAuthorizationCodeRequestUrl newAuthorizationUrl = flow.newAuthorizationUrl().setRedirectUri(REDIRECT_URL);
		return newAuthorizationUrl.build();
	}

	/** {@inheritDoc} */
	@Override public boolean isAuthorized(final String userId) throws CSystemException {
		loadCredential = flow.loadCredential(userId);
		return loadCredential != null;
	}

	/** {@inheritDoc} */
	@Override public void authenticate(final String userId, final String token) throws CSystemException {
		try {
			loadCredential = flow.loadCredential(userId);
			if (loadCredential == null) {
				GoogleAuthorizationCodeTokenRequest newTokenRequest = flow.newTokenRequest(token).setRedirectUri(REDIRECT_URL);
				GoogleTokenResponse tokenResponse = newTokenRequest.execute();
				flow.createAndStoreCredential(tokenResponse, userId);
				loadCredential = flow.loadCredential(userId);
			}
			service = Tasks.builder(transport, jsonFactory).setApplicationName(YURILO_TASKS_SAMPLE_1_0).setHttpRequestInitializer(loadCredential)
					.setJsonHttpRequestInitializer(new GoogleKeyInitializer(KEY)).build();
		} catch(Exception exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}

	/** {@inheritDoc} */
	@Override public Collection<CTaskList> getTaskLists() throws CSystemException {
		try {
			com.google.api.services.tasks.Tasks.Tasklists.List list = service.tasklists().list();
			List<TaskList> taskLists = list.execute().getItems();
			if (taskLists == null) {
				return new ArrayList<CTaskList>();
			}
			return Collections2.transform(taskLists, new Function<TaskList, CTaskList>() {
				   public CTaskList apply(TaskList t) {
				     return new CTaskList(t);
				   }
			});
		} catch (IOException exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}

	/** {@inheritDoc} */
	@Override public Collection<CTask> getTasks(final String listId) throws CSystemException {
		try {
			List<Task> tasks = service.tasks().list(listId).execute().getItems();
			if (tasks == null) {
				return new ArrayList<CTask>();
			}
			return Collections2.transform(tasks, new Function<Task, CTask>() {
				   public CTask apply(Task t) {
				     return new CTask(t);
				   }
			});
		} catch (IOException exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}

	/** {@inheritDoc} */
	@Override public CTask insertTask(final String listId, final CTask cTask) throws CSystemException {
		try {
			Task task = new Task();
			task.setTitle(cTask.getTitle());
			Insert insert = service.tasks().insert(listId, task);
			return new CTask(insert.execute());
		} catch (IOException exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}

	/** {@inheritDoc} */
	@Override public CTask updateTask(final String listId, final CTask cTask) throws CSystemException {
		try {
			Task task = service.tasks().get(listId, cTask.getId()).execute();//new Task();
			task.setId(cTask.getId());
			task.setTitle(cTask.getTitle());
			task.setNotes(cTask.getNotes());
			task.setStatus(cTask.isCompleted() ? "completed" : "needsAction");
			//TODO: fix timezone from preferences!!!
			task.setCompleted(cTask.isCompleted() ? new DateTime(new Date(), TimeZone.getDefault()) : null);
//			task.setKind("tasks#taskList");
			if (cTask.getDueDate() != null) {
				//TODO: fix timezone from preferences!!!
				task.setDue(new DateTime(cTask.getDueDate(), TimeZone.getDefault()));
			}
			Update update = service.tasks().update(listId, task.getId(), task);
			return new CTask(update.execute());
		} catch (IOException exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}

	/** {@inheritDoc} */
	@Override public void deleteTask(final String listId, final CTask cTask) throws CSystemException {
		try {
			Task task = new Task();
			task.setTitle(cTask.getTitle());
			Delete delete = service.tasks().delete(listId, cTask.getId());
			delete.execute();
		} catch (IOException exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}

	/** {@inheritDoc} */
	@Override public void deleteList(final String listId) throws CSystemException {
		try {
			com.google.api.services.tasks.Tasks.Tasklists.Delete delete = service.tasklists().delete(listId);
			delete.execute();
		} catch (IOException exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}

	/** {@inheritDoc} */
	@Override public void deleteCompleted(final String listId) throws CSystemException {
		try {
			Clear clear = service.tasks().clear(listId);
			clear.execute();
		} catch (IOException exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}

	/** {@inheritDoc} */
	@Override public CTaskList insertTaskList(final CTaskList cTaskList) throws CSystemException {
		try {
			TaskList taskList = new TaskList();
			taskList.setTitle(cTaskList.getTitle());
			com.google.api.services.tasks.Tasks.Tasklists.Insert insert = service.tasklists().insert(taskList);
			return new CTaskList(insert.execute());
		} catch (IOException exception) {
			throw new CSystemException("Failed to read the response", exception);
		}
	}
}
