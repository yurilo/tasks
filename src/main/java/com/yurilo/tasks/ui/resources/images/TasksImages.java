package com.yurilo.tasks.ui.resources.images;

import javax.swing.ImageIcon;

import com.yurilo.tasks.exceptions.CSystemException;
import com.yurilo.tasks.util.TasksUtils;

public class TasksImages {

	public static final String IMAGES_PATH = "images/";
	
	public static final ImageIcon TRAY_IMAGE = getImageIcon("evolution_tasks.gif");

	public static final ImageIcon DIALOG_ERROR_ICON = getImageIcon("dialog_warning.png");

	public static final ImageIcon VALIATION_ERROR_ICON = getImageIcon("dialog_warning_small.png");

	public static final ImageIcon NOTE_ICON = getImageIcon("note.png");

    public static ImageIcon getImageIcon(String name) {
        try {
            return new ImageIcon(TasksUtils.class.getClassLoader().getResource(IMAGES_PATH + name.trim()));
        } catch (Exception exception) {
        	throw new CSystemException("Failed to load image " + name, exception);
        }
    }

    public static ImageIcon getImage(String name, String description) {
        ImageIcon icon = getImageIcon(name);
        if(icon != null) {
            icon.setDescription(description);
        }
        return icon;
    }

}
