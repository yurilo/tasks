package com.yurilo.tasks.ui.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import com.google.inject.Inject;
import com.yurilo.tasks.service.IGoogleTasksService;
import com.yurilo.tasks.ui.IUIPackage;
import com.yurilo.tasks.ui.resources.images.TasksImages;

public class AuthDialog extends JDialog {

	private JButton authButton;

	private JTextField keyField;
	
	private JTextField loginField;
	
	private JPanel validationPanel;

	private JLabel validationLabel;

	private IGoogleTasksService service;

	private IUIPackage uiPackage;

	//TODO: double check if next line is necessary
//	Frame frame = JOptionPane.getFrameForComponent(uiPackage.getMainFrame());
	@Inject public AuthDialog(final IUIPackage uiPackage, final IGoogleTasksService service) {
        super(uiPackage.getMainFrame(), true);
        this.uiPackage = uiPackage;
        this.service = service;
        setName("authDialog");
        
        add(createGUI());

		setResizable(false);

		initializeListeners();
		uiPackage.injectResourceMap(AuthDialog.class, this);
	}

	private JPanel createGUI() {
		JPanel main = new JPanel();
		main.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));		

		JPanel loginPanel = new JPanel();
		loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.X_AXIS));
		JLabel loginLabel = new JLabel();
		loginLabel.setName("loginLabel");
		loginPanel.add(loginLabel);
		loginPanel.add(Box.createRigidArea(new Dimension(12, 0)));
		loginField = new JTextField(30);
		loginPanel.add(loginField);
		
		JPanel keyPanel = new JPanel();
		keyPanel.setLayout(new BoxLayout(keyPanel, BoxLayout.X_AXIS));
		JLabel keyLabel = new JLabel();
		keyLabel.setName("keyLabel");
		keyPanel.add(keyLabel);
		keyPanel.add(Box.createRigidArea(new Dimension(12, 0)));
		keyField = new JTextField(30);
		keyPanel.add(keyField);
		
		JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
		authButton = new JButton();
		authButton.setName("authButton");
		flow.add(authButton);
		getRootPane().setDefaultButton(authButton);

		uiPackage.makeSameSize(loginPanel, keyPanel);
//		TasksUtils.fixTextFieldSize(loginField);
//		TasksUtils.fixTextFieldSize(keyField);
		validationPanel = new JPanel();
		validationPanel.setLayout(new BoxLayout(validationPanel, BoxLayout.X_AXIS));
		validationLabel = new JLabel();
		validationLabel.setIcon(TasksImages.VALIATION_ERROR_ICON);
		validationPanel.add(validationLabel);
		validationPanel.setVisible(false);
		main.add(validationPanel);
		main.add(loginPanel);
		main.add(Box.createRigidArea(new Dimension(0, 12)));
		main.add(keyPanel);
		main.add(Box.createRigidArea(new Dimension(0, 12)));
		main.add(flow);
		return main;
	}

	private void initializeListeners() {
		authButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				if (!EmailValidator.getInstance().isValid(loginField.getText())) {
					validationError("Email address is not valid");
					return;
				}
				if (StringUtils.isBlank(keyField.getText())) {
					validationError("Code field can't be empty");
					return;
				}
				try {
					service.authenticate(loginField.getText(), keyField.getText());
					setVisible(false);
					validationPanel.setVisible(false);
				} catch (Exception exception) {
					validationError("Some error occured while calling google API: " + exception.getMessage());
				}
			}
		});
	}
	
	private void validationError(final String text) {
		validationLabel.setText(text);
		validationPanel.setVisible(true);
		this.pack();
	}
}
