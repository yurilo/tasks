package com.yurilo.tasks.ui.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.application.Action;
import org.jdesktop.application.Task;
import org.jdesktop.application.Task.BlockingScope;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.domain.CTaskList;
import com.yurilo.tasks.service.IGoogleTasksService;
import com.yurilo.tasks.ui.IUIPackage;
import com.yurilo.tasks.ui.event.TaskCreatedEvent;
import com.yurilo.tasks.ui.event.TaskCreatedListener;
import com.yurilo.tasks.ui.resources.images.TasksImages;

@Singleton public class AddTaskDialog extends JDialog {

	private JButton addButton;

	private JButton cancelButton;
	
	//TODO: use input verifier for validation: http://www.javalobby.org/java/forums/t20551.html
	private JTextField titleField;
	
	private JPanel validationPanel;

	private JLabel validationLabel;
	
	private CTaskList taskList;
	
	private TaskCreatedListener[] listeners;
	
	private IGoogleTasksService service;
	
	private IUIPackage uiPackage;
	
	private JLabel listTitleLabel;
	
	@Inject public AddTaskDialog(final IUIPackage uiPackage, final IGoogleTasksService service) {
        super(uiPackage.getMainFrame(), true);
        this.uiPackage = uiPackage;
        this.service = service;
        this.setName("addTaskDialog");

        add(createGUI());

		setResizable(false);

		initializeListeners();
		uiPackage.injectResourceMap(AddTaskDialog.class, this);
	}

	public void setTaskList(final CTaskList taskList) {
		this.taskList = taskList;
		this.listTitleLabel.setText(taskList.getTitle());
		this.titleField.setText("");
		this.titleField.requestFocus();
	}

	private JPanel createGUI() {
		JPanel main = new JPanel();
		main.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));		

		JPanel listPanel = new JPanel();
		listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.X_AXIS));
		JLabel listLabel = new JLabel();
		listLabel.setName("listLabel");
		listPanel.add(listLabel);
		listPanel.add(Box.createRigidArea(new Dimension(12, 0)));
		listTitleLabel = new JLabel();
		listPanel.add(listTitleLabel);

		
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.X_AXIS));
		JLabel titleLabel = new JLabel();
		titleLabel.setName("titleLabel");
		titlePanel.add(titleLabel);
		titlePanel.add(Box.createRigidArea(new Dimension(12, 0)));
		titleField = new JTextField(15);
		titlePanel.add(titleField);
		
		JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
		JPanel grid = new JPanel(new GridLayout(1, 2, 5, 0));
		addButton = new JButton();
		addButton.setName("addButton");
		cancelButton = new JButton();
		cancelButton.setName("cancelButton");
		grid.add(addButton);
		grid.add(cancelButton);
		flow.add(addButton);
		flow.add(cancelButton);
		getRootPane().setDefaultButton(addButton);
		
		uiPackage.makeSameSize(listPanel, titlePanel);

		validationPanel = new JPanel();
		validationPanel.setLayout(new BoxLayout(validationPanel, BoxLayout.X_AXIS));
		validationLabel = new JLabel();
		validationLabel.setIcon(TasksImages.VALIATION_ERROR_ICON);
		validationPanel.add(validationLabel);
		validationPanel.setVisible(false);
		main.add(validationPanel);
		main.add(Box.createRigidArea(new Dimension(0, 12)));
		main.add(listPanel);

		main.add(titlePanel);
		main.add(Box.createRigidArea(new Dimension(0, 12)));
		main.add(flow);
		return main;
	}

	private void initializeListeners() {
		uiPackage.addEscapeListener(this);
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setVisible(false);
			}
		});

        ActionMap map = uiPackage.getApplication().getContext().getActionMap(this);
        addButton.setAction(map.get("addTask"));
	}

	
    @Action(block = BlockingScope.ACTION)
    public Task addTask() {
		if (StringUtils.isBlank(titleField.getText())) {
			validationError("Title can't be empty");
			return null;
		}
        return new AddTaskProcess();
    }

	class AddTaskProcess extends Task<CTask, Integer> {
		
		/** The constructor. */
        public AddTaskProcess() {
            super(uiPackage.getApplication());
        }
        
		/** {@inheritDoc} */
		@Override protected CTask doInBackground() throws Exception {
			setVisible(false);
			return service.insertTask(taskList.getId(), new CTask(titleField.getText()));
		}

		/** {@inheritDoc} */
		@Override protected void succeeded(final CTask task) {
			fireTaskCreated(task);
		}

		/** {@inheritDoc} */
		@Override protected void failed(final Throwable cause) {
			super.failed(cause);
			uiPackage.reportErrorToUser("Failed to add task", cause);
		}
	}

	private void fireTaskCreated(final CTask task) {
		if (ArrayUtils.isNotEmpty(listeners)) {
			for (TaskCreatedListener listener : listeners) {
				listener.taskCreated(new TaskCreatedEvent(AddTaskDialog.class, task, taskList));
			}
		}
	}
	
	private void validationError(final String text) {
		validationLabel.setText(text);
		validationPanel.setVisible(true);
		this.pack();
	}

	public void addListeners(final TaskCreatedListener... taskCreatedListener) {
		if (ArrayUtils.isNotEmpty(listeners)) {
			return;
		}
		this.listeners = taskCreatedListener;
	}
}
