package com.yurilo.tasks.ui.view;

import javax.swing.Action;
import javax.swing.event.ListSelectionListener;

import com.google.inject.ImplementedBy;
import com.yurilo.tasks.domain.CTaskList;
import com.yurilo.tasks.ui.model.ITaskListModel;

/**
 * Task List view interface that represents interaction with the controller according to the Model-View-Presenter pattern.
 */
@ImplementedBy(TaskListsView.class)
public interface ITaskListsView {

	/**
	 * Returns selected task list.
	 * @return selected task list ot null if there is no selection.
	 */
	CTaskList getSelectedTaskList();

	/**
	 * Selects taskList.
	 * @param taskList task list to select.
	 */
	void setSelectedValue(CTaskList taskList);
	
	/**
	 * Sets model that represents task lists.
	 * @param model model that represents task lists.
	 */
	void setModel(ITaskListModel model);

	/**
	 * Selects first task list.
	 * Does nothing if there is no task lists.
	 */
	void selectFirstElement();

	void addRefreshTaskListAction(Action action);

	void addAddTaskListAction(Action action);

	void addRemoveTaskListAction(Action action);

	void addListSelectionListener(ListSelectionListener listener);

}
