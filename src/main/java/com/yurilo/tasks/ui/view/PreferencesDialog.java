package com.yurilo.tasks.ui.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.inject.Inject;
import com.yurilo.tasks.ui.IUIPackage;

public class PreferencesDialog extends JDialog {

    private JButton cancelButton;
    
    private IUIPackage uiPackage;
    
	@Inject public PreferencesDialog(final IUIPackage uiPackage) {
		super(uiPackage.getMainFrame(), false);
		this.uiPackage = uiPackage;
		setName("preferencesDialog");
		
		add(createGUI());
		setResizable(false);
		
		initializeListeners();
		uiPackage.injectResourceMap(PreferencesDialog.class, this);
	}

	private JPanel createGUI() {
		JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		main.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));

		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,10,10,10);
		c.anchor = GridBagConstraints.WEST;

		c.gridx = 0; c.gridy = 0;
		c.gridwidth = 1; c.gridheight = 1;
		JLabel accountLabel = new JLabel();
		accountLabel.setName("accountLabel");
		main.add(accountLabel, c);

		c.gridx = 0; c.gridy = 1;
		c.gridwidth = 1; c.gridheight = 1;
		JLabel localeLabel = new JLabel();
		localeLabel.setName("localeLabel");
		main.add(localeLabel, c);

		c.gridx = 0; c.gridy = 2;
		c.gridwidth = 1; c.gridheight = 1;
		JLabel timeZoneLabel = new JLabel();
		timeZoneLabel.setName("timeZoneLabel");
		main.add(timeZoneLabel, c);

		cancelButton = new JButton();
		cancelButton.setName("cancelButton");
		
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.gridx = 1;       //aligned with button 2
		c.gridy = 5;       //third row
		main.add(cancelButton, c);
		return main;
	}
	
	private void initializeListeners() {
		uiPackage.addEscapeListener(this);
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
	}
}
