package com.yurilo.tasks.ui.view;

import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.yurilo.tasks.ui.IUIPackage;
import com.yurilo.tasks.util.TasksUtils;

public class ErrorDialog extends JDialog {

    private static final int MAX_URL_LENGTH = 2000;

	private JButton cancelButton;

    private JButton sendButton;

    private JLabel errorDescriptionLabel;

    private JTextArea errorTextArea;
    
    private JScrollPane errorPane;
    
	private IUIPackage uiPackage;

	public ErrorDialog(final IUIPackage uiPackage) {
		super(uiPackage.getMainFrame(), false);
		setName("errorDialog");
        this.uiPackage = uiPackage;

		add(createGUI());
		setResizable(false);
		
		initializeListeners();
		uiPackage.injectResourceMap(ErrorDialog.class, this);
	}

	private JPanel createGUI() {
		JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		main.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));

		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,10,10,10);
		c.anchor = GridBagConstraints.WEST;

		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 0; c.gridy = 0;
		c.gridwidth = 1; c.gridheight = 2;
		JLabel imageLabel = new JLabel();
		imageLabel.setName("imageLabel");
		main.add(imageLabel, c);
		
		c.gridx = 1; c.gridy = 0;
		c.gridwidth = 1; c.gridheight = 1;
		JLabel errorLabel = new JLabel();
		errorLabel.setName("errorLabel");
		main.add(errorLabel, c);

		c.gridx = 1; c.gridy = 1;
		c.gridwidth = 1; c.gridheight = 1;
		errorDescriptionLabel = new JLabel();
		errorDescriptionLabel.setName("errorDescriptionLabel");
		main.add(errorDescriptionLabel, c);
		
		errorPane = new JScrollPane();
		errorPane.setName("errorPane");
		errorTextArea = new JTextArea();
		errorTextArea.setColumns(40);
		errorTextArea.setEditable(false);
		errorTextArea.setRows(5);
		errorTextArea.setName("errorTextArea");

		errorPane.setViewportView(errorTextArea);
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.gridx = 0; c.gridy = 3;
		c.gridwidth = 2; c.gridheight = 1;
		main.add(errorPane, c);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		sendButton = new JButton();
		sendButton.setName("sendButton");
		cancelButton = new JButton();
		cancelButton.setName("cancelButton");
		buttonPanel.add(sendButton);
		buttonPanel.add(cancelButton);

		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.SOUTHEAST; //bottom of space
		c.gridx = 1; c.gridy = 4;
		main.add(buttonPanel, c);
		return main;
	}
	
	public void setError(final String error, final String extendedError) {
		this.errorDescriptionLabel.setText(error);
		if (extendedError == null) {
			this.errorPane.setVisible(false);
			this.sendButton.setVisible(false);
		} else {
			this.sendButton.setVisible(true);
			this.errorPane.setVisible(true);
			this.errorTextArea.setText(extendedError);
		}
	}
	
	private void initializeListeners() {
		uiPackage.addEscapeListener(this);
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setVisible(false);
			}
		});
		sendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
		        Desktop desktop = Desktop.getDesktop();
		        try {
		            String message = errorDescriptionLabel.getText() + " " + errorTextArea.getText();
		            String encodedBody = URLEncoder.encode(message, "UTF-8");
		            if (encodedBody.length() > MAX_URL_LENGTH) {
		            	encodedBody = encodedBody.substring(0, MAX_URL_LENGTH);
		            }
		            URI uri = URI.create("mailto:" + TasksUtils.getAdminEmail() + "?subject=Tasks%20Error&body=" + encodedBody);
		            desktop.mail(uri);
		        } catch (IOException exception) {
		        	uiPackage.reportErrorToUser("Failed to generate email", exception);
		        }

			}
		});
	}
}
