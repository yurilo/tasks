package com.yurilo.tasks.ui.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.yurilo.tasks.ui.IUIPackage;

@Singleton public class AboutDialog extends JDialog {

    private JButton cancelButton;
    
    private IUIPackage uiPackage;
    
	@Inject public AboutDialog(final IUIPackage uiPackage) {
		super(uiPackage.getMainFrame(), false);
		this.uiPackage = uiPackage;
		setName("aboutDialog");
		
		add(createGUI());
		setResizable(false);
		
		initializeListeners();
		uiPackage.injectResourceMap(AboutDialog.class, this);
	}

	private JPanel createGUI() {
		JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		main.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));

		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,10,10,10);
		c.anchor = GridBagConstraints.WEST;

		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 0; c.gridy = 0;
		c.gridwidth = 1; c.gridheight = 3;
		JLabel imageLabel = new JLabel();
		imageLabel.setName("imageLabel");
		main.add(imageLabel, c);
		
		c.gridx = 1; c.gridy = 0;
		c.gridwidth = 1; c.gridheight = 1;
		JLabel nameLabel = new JLabel();
		nameLabel.setName("nameLabel");
		main.add(nameLabel, c);

		c.gridx = 1; c.gridy = 1;
		c.gridwidth = 1; c.gridheight = 1;
		JLabel versionLabel = new JLabel();
		versionLabel.setName("versionLabel");
		main.add(versionLabel, c);

		c.gridx = 1; c.gridy = 2;
		c.gridwidth = 1; c.gridheight = 1;
		JLabel informationLabel = new JLabel();
		informationLabel.setName("informationLabel");
		main.add(informationLabel, c);

		c.gridx = 0; c.gridy = 3;
		c.gridwidth = 2; c.gridheight = 1;
		JLabel copyrightLabel = new JLabel();
		copyrightLabel.setName("copyrightLabel");
		main.add(copyrightLabel, c);

		c.gridx = 0; c.gridy = 4;
		c.gridwidth = 2; c.gridheight = 1;
		JLabel rightsLabel = new JLabel();
		rightsLabel.setName("rightsLabel");
		main.add(rightsLabel, c);

		cancelButton = new JButton();
		cancelButton.setName("cancelButton");
		
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.gridx = 1; c.gridy = 5;
		main.add(cancelButton, c);
		return main;
	}
	
	private void initializeListeners() {
        this.addMouseListener(new MouseAdapter() { // remove about window on click
            @Override
            public void mouseClicked(MouseEvent e) {
            	AboutDialog.this.setVisible(false);
            }
        });
		uiPackage.addEscapeListener(this);
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
	}
}
