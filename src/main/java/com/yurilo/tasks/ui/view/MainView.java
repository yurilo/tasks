package com.yurilo.tasks.ui.view;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;

import org.jdesktop.application.FrameView;

import com.yurilo.tasks.ui.IUIPackage;
import com.yurilo.tasks.ui.TasksApplication;
import com.yurilo.tasks.ui.controller.ITaskListsController;
import com.yurilo.tasks.ui.resources.images.TasksImages;

@Singleton public class MainView extends FrameView implements IMainView {

	private static final String SEPARATOR = "---";

	private IUIPackage uiPackage;

	private ITaskListsView listsView;

	private ITasksView tasksView;
	
	@Inject public MainView(final ITasksView tasksView, final ITaskListsView taskListsView, final IUIPackage uiPackage) {
		super(uiPackage.getApplication());
		this.uiPackage = uiPackage;
		this.listsView = taskListsView;
		this.tasksView = tasksView;
	}

	@Override public void init() {
		// add main views
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		mainPanel.add((Component) listsView, BorderLayout.WEST);
		mainPanel.add((Component) tasksView, BorderLayout.CENTER);
		uiPackage.injectResourceMap(tasksView.getClass(), mainPanel);
		uiPackage.injectResourceMap(listsView.getClass(), mainPanel);
		this.setComponent(mainPanel);
		// initialize menu bar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(createMainMenu("menu.main", ITaskListsController.ACTION_PRINT, SEPARATOR, TasksApplication.ACTION_QUIT));
		menuBar.add(createMainMenu("menu.edit", ITaskListsController.ACTION_REFRESH, SEPARATOR, ITaskListsController.ACTION_LIST_ADD, ITaskListsController.ACTION_TASK_ADD, SEPARATOR, ITaskListsController.ACTION_TASK_REMOVE, ITaskListsController.ACTION_CLEAN, SEPARATOR, TasksApplication.ACTION_PREFERENCES));
		menuBar.add(createMainMenu("menu.help", SEPARATOR, TasksApplication.ACTION_ABOUT));
		this.setMenuBar(menuBar);
		
		// add status bar 
		StatusBar statusBar = new StatusBar(uiPackage.getApplication(), uiPackage.getApplication().getContext().getTaskMonitor());
    	this.setStatusBar(statusBar);
    	
    	// initialize system tray
		if (SystemTray.isSupported()) { // check to see if system tray is supported on OS.
			initializeSystemTray();
		} else {
			uiPackage.reportErrorToUser("System Tray unsupported!");
		}
		getFrame().setIconImage(TasksImages.TRAY_IMAGE.getImage());
		getFrame().setMinimumSize(new Dimension(800, 400));
		getFrame().setVisible(true);
	}

	private void initializeSystemTray() {
		SystemTray sysTray = null;
		try {
			sysTray = SystemTray.getSystemTray();
		} catch (SecurityException exception) {
			uiPackage.reportErrorToUser("You don't have right to access system tray");
		} catch (Exception exception) {
			uiPackage.reportErrorToUser("Failed to initialize system tray!");
		}
		final JPopupMenu popupMenu = createPopupMenu("menu.popup.tray", TasksApplication.ACTION_ABOUT, SEPARATOR, TasksApplication.ACTION_QUIT);
		TrayIcon trayIcon = new TrayIcon(TasksImages.TRAY_IMAGE.getImage(), "Tasks");
		trayIcon.setImageAutoSize(true); // scale icon to tray size.
		trayIcon.addMouseListener(new MouseAdapter() {
			@Override public void mouseReleased(final MouseEvent event) {
			}

			@Override public void mousePressed(final MouseEvent event) {
				if (event.isPopupTrigger()) {
					popupMenu.setInvoker(getFrame());
					popupMenu.setLocation(event.getX(), event.getY());
					popupMenu.setVisible(true);
				} else {
					popupMenu.setVisible(false);
				}
//				mouseReleased(event);
			}

			@Override public void mouseClicked(final MouseEvent event) {
				getFrame().setVisible(!getFrame().isVisible());
			}
			
		});
		try {
			sysTray.add(trayIcon);
		} catch (AWTException e) {
			uiPackage.reportErrorToUser("System Tray unsupported!");
		}
	}

    private JMenu createMainMenu(final String menuName, final String... actionNames) {
		JMenu menu = new JMenu();
		menu.setName(menuName);
		populateMenu(menu, actionNames);
		return menu;
    }

    private JPopupMenu createPopupMenu(final String menuName, final String... actionNames) {
		JPopupMenu menu = new JPopupMenu();
		menu.setName(menuName);
		populateMenu(menu, actionNames);
		return menu;
    }

	private void populateMenu(final JComponent menu, final String... actionNames) {
		int modifier = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		for (String actionName : actionNames) {
			if (actionName.equals(SEPARATOR)) {
				if (menu instanceof JMenu) {
					((JMenu) menu).addSeparator();
				}
				if (menu instanceof JPopupMenu) {
					((JPopupMenu) menu).addSeparator();
				}
				continue;
			}
			JMenuItem menuItem = new JMenuItem();
			menuItem.setAction(uiPackage.getApplication().getActionMap().get(actionName));
			KeyStroke keyStroke = menuItem.getAccelerator();
			if (keyStroke != null) {
				menuItem.setAccelerator(KeyStroke.getKeyStroke(keyStroke.getKeyCode(), modifier));
			}
			menu.add(menuItem);
		}
	}

}
