package com.yurilo.tasks.ui.view.table;

public interface TableButtonListener {
	void tableButtonClicked(int row, int col);
}
