package com.yurilo.tasks.ui.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.google.inject.Inject;
import com.yurilo.tasks.ui.IUIPackage;

public class EditTaskDialog extends JDialog {

    private JButton cancelButton;

    private JButton saveButton;

    private JTextArea noteTextArea;
    
    private JScrollPane notePane;
    
	private IUIPackage uiPackage;

	@Inject public EditTaskDialog(final IUIPackage uiPackage) {
		super(uiPackage.getMainFrame(), false);
		setName("editTaskDialog");
        this.uiPackage = uiPackage;

		add(createGUI());
		setResizable(false);
		
		initializeListeners();
		uiPackage.injectResourceMap(EditTaskDialog.class, this);
	}

	private JPanel createGUI() {
		JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		main.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));

		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,10,10,10);
		c.anchor = GridBagConstraints.WEST;

		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		JLabel imageLabel = new JLabel();
		imageLabel.setName("noteLabel");
		main.add(imageLabel, c);
		
		notePane = new JScrollPane();
		notePane.setName("notePane");
		noteTextArea = new JTextArea();
		noteTextArea.setColumns(20);
		noteTextArea.setEditable(true);
		noteTextArea.setRows(10);
		noteTextArea.setName("errorTextArea");

		notePane.setViewportView(noteTextArea);
		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.PAGE_END; //bottom of space
		c.gridx = 1;       //aligned with button 2
		c.gridy = 0;       //third row
		c.gridwidth = 1;
		c.gridheight = 1;
		main.add(notePane, c);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		saveButton = new JButton();
		saveButton.setName("saveButton");
        ActionMap map = uiPackage.getApplication().getContext().getActionMap();
        saveButton.setAction(map.get("editTask"));

		cancelButton = new JButton();
		cancelButton.setName("cancelButton");
		buttonPanel.add(saveButton);
		buttonPanel.add(cancelButton);
		getRootPane().setDefaultButton(saveButton);

		c.ipady = 0;       //reset to default
		c.weighty = 1.0;   //request any extra vertical space
		c.anchor = GridBagConstraints.SOUTHEAST; //bottom of space
		c.gridx = 1;       //aligned with button 2
		c.gridy = 1;       //third row
		main.add(buttonPanel, c);
		return main;
	}
		
	private void initializeListeners() {
		uiPackage.addEscapeListener(this);
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setVisible(false);
			}
		});
	}
	
	public void setTextNote(final String text) {
		this.noteTextArea.setText(text);
	}

	public String getTextNote() {
		return this.noteTextArea.getText();
	}

	public void addSaveListener(final ActionListener actionListener) {
		saveButton.addActionListener(actionListener);
	}
	
	public void show() {
		uiPackage.showDialog(this);
	}
}
