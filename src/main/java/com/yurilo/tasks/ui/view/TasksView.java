package com.yurilo.tasks.ui.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.text.MessageFormat;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.jdesktop.swingx.table.DatePickerCellEditor;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.domain.CTaskList;
import com.yurilo.tasks.ui.IUIPackage;
import com.yurilo.tasks.ui.model.ITaskTableModel;
import com.yurilo.tasks.ui.resources.images.TasksImages;
import com.yurilo.tasks.util.TasksUtils;

@Singleton
public class TasksView extends JComponent implements ITasksView {
	
	private JTable table;

	private JButton addTaskButton;
	private JButton removeTaskButton;
	private JButton removeCompletedTaskButton;

	private ITaskTableModel model;
	
	private IUIPackage uiPackage;
	
	@Inject public TasksView(final IUIPackage uiPackage, final ITaskTableModel model) {
		this.model = model;
		this.uiPackage = uiPackage;
        table = new JTable();
        
		JPanel listPane = new JPanel();
		Border border = BorderFactory.createLineBorder(Color.black);
		this.setBorder(border);
		listPane.setLayout(new BorderLayout());

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
		addTaskButton = new JButton();
		addTaskButton.setName("addTaskButton");
		removeTaskButton = new JButton();
		removeTaskButton.setName("removeTaskButton");
		removeCompletedTaskButton = new JButton();
		removeCompletedTaskButton.setName("removeCompletedTaskButton");
		buttonPane.add(addTaskButton);
		buttonPane.add(removeTaskButton);
		buttonPane.add(removeCompletedTaskButton);
		listPane.add(buttonPane, BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
		listPane.add(scrollPane, BorderLayout.CENTER);

		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		add(listPane, BorderLayout.CENTER);

		table.setModel(model);
		setWidthAsPercentages(table, 0.05, 0.5, 0.2, 0.25);
        TableColumn dueDateColumn = table.getColumn("Due date");
        DatePickerCellEditor dueDateCellEditor = new DatePickerCellEditor();
        dueDateCellEditor.setClickCountToStart(1);
		dueDateColumn.setCellEditor(dueDateCellEditor);
		TableColumn notesColumn = table.getColumn("Notes");
		notesColumn.setCellRenderer(new DefaultTableCellRenderer() {
			@Override public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
					int column) {
				Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (value == null) {
					return component;
				}
				JLabel label = (JLabel) component;
				String cellText = "<html>" + TasksUtils.insertPeriodically(value.toString(), "<br>", 30) + "</html>";
				label.setToolTipText(cellText);
				return label;
			}
		});
		notesColumn.setCellEditor(new NotesEditor());
//		table.setDefaultRenderer(Object.class, new EnableWrapperTableCellRenderer(table.getDefaultRenderer(Object.class)));
	}

	private static void setWidthAsPercentages(final JTable table, double... percentages) {
		final double factor = 10000;
		TableColumnModel model = table.getColumnModel();
		for (int columnIndex = 0; columnIndex < percentages.length; columnIndex++) {
			TableColumn column = model.getColumn(columnIndex);
			column.setPreferredWidth((int) (percentages[columnIndex] * factor));
		}
	}

	public class NotesEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
		protected static final String EDIT = "edit";

		String noteText;

		JButton button;

		JDialog dialog;

		JTextArea textArea;

		JPanel panel;

		public NotesEditor() {
			button = new JButton();
			button.setActionCommand(EDIT);
			button.addActionListener(this);
			button.setBorderPainted(false);

			panel = new JPanel();
			panel.setLayout(new GridBagLayout());
			panel.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));

			JLabel notesLabel = new JLabel("Notes:");
			textArea = new JTextArea();
			textArea.setColumns(20);
			textArea.setRows(10);
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
			
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor = GridBagConstraints.WEST;

			c.fill = GridBagConstraints.VERTICAL;
			c.gridx = 0; c.gridy = 0;
			c.gridwidth = 1; c.gridheight = 1;
			panel.add(notesLabel, c);

			c.gridx = 0; c.gridy = 1;
			c.gridwidth = 1; c.gridheight = 1;
			panel.add(textArea, c);
		}

		public void actionPerformed(final ActionEvent event) {
			if (EDIT.equals(event.getActionCommand())) {
				CTask selectedTask = TasksView.this.getSelectedTask();
				textArea.setText(selectedTask.getNotes());
				int okStatus = JOptionPane.showConfirmDialog(SwingUtilities.getWindowAncestor(TasksView.this), panel, "Edit Notes",
						JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, TasksImages.NOTE_ICON);
				if (okStatus == JOptionPane.OK_OPTION) {
					noteText = textArea.getText();
					selectedTask.setNotes(noteText);
				}
				fireEditingStopped();
			}
		}

		public Object getCellEditorValue() {
			return noteText;
		}

		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			noteText = (String) value;
			return button;
		}
	}

	/** {@inheritDoc} */
	@Override public CTask getSelectedTask() {
		int selectedRow = table.getSelectedRow();
		if (selectedRow < 0) {
			return null;
		}
		return model.getValue(selectedRow);
	}

	/** {@inheritDoc} */
	@Override public void clearTasksSelection() {
		if (table.getCellEditor() != null) {
			table.getCellEditor().cancelCellEditing();
		}
		table.clearSelection();
	}
	
	
	/** {@inheritDoc} */
	@Override public void addAddTaskListener(final ActionListener listener) {
		addTaskButton.addActionListener(listener);
	}

	/** {@inheritDoc} */
	@Override public void addRemoveTaskAction(final Action action) {
		removeTaskButton.setAction(action);
	}

	/** {@inheritDoc} */
	@Override public void addRemoveCompletedTasksAction(final Action action) {
		removeCompletedTaskButton.setAction(action);
	}

	/** {@inheritDoc} */
	@Override public void addTableModelListener(final TableModelListener listener) {
		table.getModel().addTableModelListener(listener);
	}

	/** {@inheritDoc} */
	@Override public void print(final CTaskList cTaskList) {
		JTable.PrintMode printMode = JTable.PrintMode.FIT_WIDTH;
		HashPrintRequestAttributeSet praSet = new HashPrintRequestAttributeSet();
		praSet.add(OrientationRequested.PORTRAIT);
		String headerMessage = "Tasks";
		if (cTaskList != null) {
			headerMessage = cTaskList.getTitle();
		}
		try {
			table.print(printMode, new MessageFormat(headerMessage), null, true, praSet, true);
		} catch (PrinterException exception) {
			uiPackage.reportErrorToUser("Failed to print task list", exception);
		}
	}

}
