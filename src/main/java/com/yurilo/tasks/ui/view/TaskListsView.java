package com.yurilo.tasks.ui.view;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

import com.google.inject.Singleton;
import com.yurilo.tasks.domain.CTaskList;
import com.yurilo.tasks.ui.model.ITaskListModel;

@Singleton
public class TaskListsView extends JComponent implements ITaskListsView {

	private JList tasksLists;

	private JButton addTaskListButton;

	private JButton refreshTaskListButton;

	private JButton removeTaskListButton;

	public TaskListsView() {
		super();
        tasksLists = new JList();
        tasksLists.setName("taskLists");
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
        tasksLists.setCellRenderer(new DefaultListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				label.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
				CTaskList taskList = (CTaskList) value;
				setText(taskList.getTitle());
				return label;
			}
		});
        tasksLists.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tasksLists.setSelectedIndex(0);
        tasksLists.setVisibleRowCount(5);
        
        addTaskListButton = new JButton();
        addTaskListButton.setName("addTaskListButton");
        refreshTaskListButton = new JButton();
        refreshTaskListButton.setName("refreshTaskListButton");
        removeTaskListButton = new JButton();
        removeTaskListButton.setName("removeTaskListButton");
        
		setLayout(new BorderLayout());
		JPanel listPane = new JPanel();
		listPane.setLayout(new BoxLayout(listPane, BoxLayout.Y_AXIS));
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
		buttonPane.add(addTaskListButton);
		buttonPane.add(refreshTaskListButton);
		buttonPane.add(removeTaskListButton);
		listPane.add(buttonPane);
		JScrollPane scrollPane = new JScrollPane(tasksLists);
		scrollPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
		listPane.add(scrollPane);
		add(listPane, BorderLayout.CENTER);
	}

	/** {@inheritDoc} */
	@Override public CTaskList getSelectedTaskList() {
		return (CTaskList) tasksLists.getSelectedValue();
	}

	/** {@inheritDoc} */
	@Override public void setSelectedValue(final CTaskList taskList) {
		tasksLists.setSelectedValue(taskList, true);
	}

	/** {@inheritDoc} */
	@Override public void setModel(final ITaskListModel model) {
		tasksLists.setModel(model);
	}

	/** {@inheritDoc} */
	@Override public void selectFirstElement() {
		tasksLists.setSelectedIndex(0);
	}

	/** {@inheritDoc} */
	@Override public void addRefreshTaskListAction(final Action action) {
		refreshTaskListButton.setAction(action);
	}

	/** {@inheritDoc} */
	@Override public void addAddTaskListAction(final Action action) {
		addTaskListButton.setAction(action);
	}

	/** {@inheritDoc} */
	@Override public void addRemoveTaskListAction(Action action) {
		removeTaskListButton.setAction(action);
	}

	/** {@inheritDoc} */
	@Override public void addListSelectionListener(final ListSelectionListener listener) {
		tasksLists.addListSelectionListener(listener);
	}

}
