package com.yurilo.tasks.ui.view;

import com.google.inject.ImplementedBy;

/**
 * Main view that contains description of the MenuBar, Tray and Main Frame.
 */
@ImplementedBy(MainView.class)
public interface IMainView {

	void init();

}
