package com.yurilo.tasks.ui.view;

import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.event.TableModelListener;

import com.google.inject.ImplementedBy;
import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.domain.CTaskList;

/**
 * Tasks view interface that represents interaction with the controller according to the Model-View-Presenter pattern.
 */
@ImplementedBy(TasksView.class)
public interface ITasksView {

	/**
	 * Returns selected task
	 * @return selected task or null is there is no selection.
	 */
	CTask getSelectedTask();
	
	void addAddTaskListener(ActionListener listener);

	void addRemoveTaskAction(Action action);

	void addRemoveCompletedTasksAction(Action action);

	void addTableModelListener(TableModelListener listener);

	void print(CTaskList cTaskList);

	void clearTasksSelection();

}