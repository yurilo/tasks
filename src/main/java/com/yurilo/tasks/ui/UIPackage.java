package com.yurilo.tasks.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.inject.Singleton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceManager;
import org.jdesktop.application.ResourceMap;

import com.yurilo.tasks.ui.view.ErrorDialog;

/**
 * Handles application context.
 */
@Singleton public final class UIPackage implements IUIPackage {
	
    /** The main Tasks frame. */
    private ApplicationContext appContext;

    public UIPackage(final ApplicationContext appContext) {
    	this.appContext = appContext;
	}

    /** {@inheritDoc} */
    @Override public JFrame getMainFrame() {
    	return ((TasksApplication) appContext.getApplication()).getMainFrame();
    }

    /** {@inheritDoc} */
    @Override public TasksApplication getApplication() {
		return (TasksApplication) appContext.getApplication();
	}

    /** {@inheritDoc} */
    @Override public void showDialog(final JDialog dialog) {
		dialog.setLocationRelativeTo(getMainFrame());
		//TODO: add multi-monitor support
//		Point location = getMainFrame().getLocation();
//		dialog.setLocation((int) location.getX(), (int) location.getY());
//		dialog.setLocationByPlatform(false);
        getApplication().show(dialog);
    }

    /** {@inheritDoc} */
    @Override public void injectResourceMap(@SuppressWarnings("rawtypes") final Class clazz, final Component component) {
		ResourceMap resourceMap = getResourceMap(clazz);
		if (resourceMap != null) {
			resourceMap.injectComponents(component);
		}
    }
    
    /** {@inheritDoc} */
    @Override public void addEscapeListener(final JDialog dialog) {
		ActionListener escListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
			}
		};
		dialog.getRootPane().registerKeyboardAction(escListener, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
	}
    
    /** {@inheritDoc} */
    @Override public void makeSameSize(JComponent... cs) {
		Dimension maxSize = cs[0].getPreferredSize();
		for (JComponent c : cs) {
			if (c.getPreferredSize().width > maxSize.width) {
				maxSize = c.getPreferredSize();
			}
		}
		for (JComponent c : cs) {
			c.setPreferredSize(maxSize);
			c.setMinimumSize(maxSize);
			c.setMaximumSize(maxSize);
		}
	}

    /** {@inheritDoc} */
    @Override public void reportErrorToUser(String errorMsg) {
        reportErrorToUser(errorMsg, null);
    }   

    /** {@inheritDoc} */
    @Override public void reportErrorToUser(final String errorMsg, final Throwable exception) {
    	final String fullStackTrace = (exception == null) ? StringUtils.EMPTY : "Stack Trace:\n" + ExceptionUtils.getFullStackTrace(exception);
    	final String title = (errorMsg == null) ? "Some unknown error" : errorMsg;
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	ErrorDialog errorDialog = new ErrorDialog(UIPackage.this);
            	errorDialog.setError(title, fullStackTrace);
            	showDialog(errorDialog);
            }
        });
    }
    
    @SuppressWarnings("rawtypes") private ResourceMap getResourceMap(final Class clazz) {
    	try {
			ApplicationContext ctxt = Application.getInstance().getContext();
			ResourceManager mgr = ctxt.getResourceManager();
			ResourceMap resource = mgr.getResourceMap(clazz);
			return resource;
    	} catch (IllegalStateException exception) {
    		return null;
    	}
	}

}
