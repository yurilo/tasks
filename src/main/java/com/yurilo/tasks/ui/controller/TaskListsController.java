package com.yurilo.tasks.ui.controller;

import java.awt.event.ActionEvent;
import java.util.Collection;

import javax.inject.Provider;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.jdesktop.application.Action;
import org.jdesktop.application.Task;
import org.jdesktop.application.Task.BlockingScope;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.domain.CTaskList;
import com.yurilo.tasks.service.IGoogleTasksService;
import com.yurilo.tasks.ui.IUIPackage;
import com.yurilo.tasks.ui.event.TaskCreatedEvent;
import com.yurilo.tasks.ui.event.TaskCreatedListener;
import com.yurilo.tasks.ui.event.TaskListCreatedEvent;
import com.yurilo.tasks.ui.event.TaskListCreatedListener;
import com.yurilo.tasks.ui.model.IModel;
import com.yurilo.tasks.ui.model.ITaskListModel;
import com.yurilo.tasks.ui.model.ITaskTableModel;
import com.yurilo.tasks.ui.model.TasksTableModel;
import com.yurilo.tasks.ui.view.AddTaskDialog;
import com.yurilo.tasks.ui.view.AddTaskListDialog;
import com.yurilo.tasks.ui.view.EditTaskDialog;
import com.yurilo.tasks.ui.view.ITaskListsView;
import com.yurilo.tasks.ui.view.ITasksView;

@Singleton
public class TaskListsController implements TaskListCreatedListener, TaskCreatedListener, ITaskListsController {

	private ITaskListsView taskListsView;
	private ITasksView tasksView;

	private IGoogleTasksService service;
	private ITaskTableModel tasksTableModel;
	private ITaskListModel taskListsModel;

	private IUIPackage uiPackage;

	@Inject private Provider<AddTaskListDialog> addTaskListDialogProvider;

	@Inject private Provider<AddTaskDialog> addTaskDialogProvider;

	@Inject public TaskListsController(final IUIPackage uiPackage, final IGoogleTasksService service, final IModel model, final ITaskListsView taskListsView, final ITasksView tasksView) {
		this.service = service;
		this.taskListsModel = model.getTaskListsModel();
		this.tasksTableModel = model.getTasksTableModel();
		this.taskListsView = taskListsView;
		this.taskListsView.setModel(model.getTaskListsModel());
		this.tasksView = tasksView;
		this.uiPackage = uiPackage;
        initializeActions();
	}

	public void initializeActions() {
		final ActionMap map = uiPackage.getApplication().getContext().getActionMap(TaskListsController.class, this);
        taskListsView.addRefreshTaskListAction(map.get(ACTION_REFRESH));
        taskListsView.addRemoveTaskListAction(map.get("removeTaskList"));
        taskListsView.addAddTaskListAction(new AbstractAction() {
			@Override public void actionPerformed(ActionEvent e) {
				addTaskList();
			}
		});
        taskListsView.addListSelectionListener(new ListSelectionListener() {
			@Override public void valueChanged(final ListSelectionEvent e) {
				tasksView.clearTasksSelection();
				javax.swing.Action action = map.get("refreshTasks");
				action.actionPerformed(new ActionEvent(e.getSource(), 1, "refreshTasks"));
			}
		});        
        
		tasksView.addAddTaskListener(new AbstractAction() {
			public void actionPerformed(final ActionEvent e) {
				addTask();
			}
		});
        tasksView.addRemoveTaskAction(map.get(ACTION_TASK_REMOVE));
		tasksView.addRemoveCompletedTasksAction(map.get(ACTION_CLEAN));
		
		tasksView.addTableModelListener(new TableModelListener() {
			@Override public void tableChanged(final TableModelEvent event) {
				int row = event.getFirstRow();
				int column = event.getColumn();
				CTaskList selectedTaskList = taskListsView.getSelectedTaskList();
				if (column < 0 || selectedTaskList == null) {
					return;
				}
				TasksTableModel model = (TasksTableModel) event.getSource();
				CTask task = model.getValue(row);
				String listId = selectedTaskList.getId();
				editTask(listId, task).execute();
			}
		});
	}
	
	/** {@inheritDoc} */
	@Override @Action(name = ACTION_REFRESH, block = BlockingScope.ACTION) public Task refreshTaskLists() {
        return new RefreshTaskListsProcess();
    }

	/** {@inheritDoc} */
	@Override @Action(block = BlockingScope.ACTION) public Task refreshTasks() {
	    CTaskList item = taskListsView.getSelectedTaskList();
	    return new RefreshTasksProcess(item);
    }

	/** {@inheritDoc} */
	@Override @Action(block = BlockingScope.ACTION) public Task editTask(final String listId, final CTask task) {
		return new EditTaskProcess(task, listId);
    }

	/** {@inheritDoc} */
	@Override @Action(block = BlockingScope.COMPONENT) public Task removeTaskList() {
		CTaskList selectedValue = taskListsView.getSelectedTaskList();
		if (selectedValue == null) {
			return null;
		}
		Object[] options = { "Yes", "Cancel" };
		int n = JOptionPane.showOptionDialog(uiPackage.getMainFrame(), "Are you shure you want to delete this task list?", "Question",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
		if (n == JOptionPane.OK_OPTION) {
			return new RemoveTaskListProcess(selectedValue);
		}
		return null;
	}

	/** {@inheritDoc} */
	@Override @Action(name = ACTION_TASK_REMOVE, block = BlockingScope.COMPONENT) public Task removeTask() {
		CTask selectedTask = tasksView.getSelectedTask();
		if (selectedTask == null) {
			return null;
		}
		return new RemoveTaskProcess(selectedTask);
	}

	/** {@inheritDoc} */
	@Override @Action(name = ACTION_CLEAN, block = BlockingScope.COMPONENT) public Task removeCompletedTasks() {
		return new RemoveCompletedTasksProcess();
	}

	/** {@inheritDoc} */
	@Override @Action(name = ACTION_LIST_ADD, block = BlockingScope.COMPONENT) public void addTaskList() {
		AddTaskListDialog addTaskListDialog = addTaskListDialogProvider.get();
		addTaskListDialog.addListeners(TaskListsController.this);
		uiPackage.showDialog(addTaskListDialog);
	}

	/** {@inheritDoc} */
	@Override @Action(name = ACTION_TASK_ADD, block = BlockingScope.COMPONENT) public void addTask() {
		CTaskList selectedValue = taskListsView.getSelectedTaskList();
		if (selectedValue == null) {
			return;
		}
		AddTaskDialog addTaskDialog = addTaskDialogProvider.get();
		addTaskDialog.setTaskList(selectedValue);
		addTaskDialog.addListeners(TaskListsController.this);
		uiPackage.showDialog(addTaskDialog);
	}

	/** {@inheritDoc} */
	@Override public void taskListCreated(final TaskListCreatedEvent event) {
		taskListsModel.addElement(event.getTaskList());
		taskListsView.setSelectedValue(event.getTaskList());
//		refreshAll();
	}
	
	/** {@inheritDoc} */
	@Override public void taskCreated(final TaskCreatedEvent event) {
		tasksTableModel.addRow(event.getTask());
//		populateTasksModel(event.getTaskList());
	}

	/** {@inheritDoc} */
	@Override @Action(name = ACTION_PRINT) public void printTaskList() {
		tasksView.print(taskListsView.getSelectedTaskList());
	}

    @Override
    public ActionMap getActionMap () {
        return uiPackage.getApplication().getContext().getActionMap(TaskListsController.class, this);
    }    
    
	/**
	 * Updates Task.<p>
	 * Updates UI if task is successfully updated on google server, shows error dialog with stack trace otherwise. 
	 */
	class EditTaskProcess extends Task<CTask, Integer> {
		
		private CTask task;
		
		private String listId;
		
		/** The constructor. */
		public EditTaskProcess(final CTask task, final String listId) {
			super(uiPackage.getApplication());
			this.task = task;
			this.listId = listId;
		}

		/** {@inheritDoc} */
		@Override protected CTask doInBackground() throws Exception {
			return service.updateTask(listId, task);
		}

		/** {@inheritDoc} */
		@Override protected void succeeded(final CTask task) {
			try {
				//TODO: set returned value!!!
				uiPackage.getMainFrame().pack();
			} catch (Exception exception) {
				uiPackage.reportErrorToUser("Failed to process operation", exception);
			}
		}

		/** {@inheritDoc} */
		@Override protected void failed(final Throwable cause) {
			super.failed(cause);
			uiPackage.reportErrorToUser("Failed to update task " + task.getTitle(), cause);
		}
	}

	/**
	 * Refreshes Task Lists.<p>
	 * Updates UI if task lists are successfully returned from google server, shows error dialog with stack trace otherwise. 
	 */
	class RefreshTaskListsProcess extends Task<Collection<CTaskList>, Integer> {
		
		/** The constructor. */
        public RefreshTaskListsProcess() {
            super(uiPackage.getApplication());
        }
        
		/** {@inheritDoc} */
		@Override protected Collection<CTaskList> doInBackground() throws Exception {
			return service.getTaskLists();
		}

		/** {@inheritDoc} */
		@Override protected void succeeded(final Collection<CTaskList> taskLists) {
			try {
				CTaskList selectedTaskList = taskListsView.getSelectedTaskList();
				taskListsModel.removeAllElements();
				for (CTaskList taskList : taskLists) {
					taskListsModel.addElement(taskList);
				}
				if (taskListsModel.contains(selectedTaskList)) {
					taskListsView.setSelectedValue(selectedTaskList);
				} else {
					taskListsView.selectFirstElement();
				}
			} catch (Exception exception) {
				uiPackage.reportErrorToUser("Failed to process operation", exception);
			}
		}

		/** {@inheritDoc} */
		@Override protected void failed(final Throwable cause) {
			super.failed(cause);
			uiPackage.reportErrorToUser("Failed to refresh task lists", cause);
		}
	}

	/**
	 * Refreshes Tasks associated with task list.<p>
	 * Updates UI if tasks are successfully returned from google server, shows error dialog with stack trace otherwise. 
	 */
	class RefreshTasksProcess extends Task<Collection<CTask>, Integer> {
		
		private CTaskList taskList; 
		
		/** The constructor. */
		public RefreshTasksProcess(final CTaskList taskList) {
            super(uiPackage.getApplication());
			this.taskList = taskList;
		}

		/** {@inheritDoc} */
		@Override protected Collection<CTask> doInBackground() throws Exception {
			if (taskList == null) {
				cancel(true);
				return null;
			}
			return service.getTasks(taskList.getId());
		}

		/** {@inheritDoc} */
		@Override protected void succeeded(final Collection<CTask> tasks) {
			tasksTableModel.cleanAll();
			tasksTableModel.addRows(tasks);
//			UIPackage.getInstance().getMainFrame().pack();
		}

		/** {@inheritDoc} */
		@Override protected void failed(final Throwable cause) {
			super.failed(cause);
			uiPackage.reportErrorToUser("Failed to refresh tasks", cause);
		}

	}

	/**
	 * Removes Task List from google account.<p>
	 * Updates UI if task list is successfully deleted, shows error dialog with stack trace otherwise. 
	 */
	class RemoveTaskListProcess extends Task<CTaskList, Integer> {
		
		private CTaskList taskList;
		
		/** The constructor. */
		public RemoveTaskListProcess(final CTaskList taskList) {
            super(uiPackage.getApplication());
            this.taskList = taskList;
		}

		/** {@inheritDoc} */
		@Override protected CTaskList doInBackground() throws Exception {
			service.deleteList(taskList.getId());
			return taskList;
		}

		/** {@inheritDoc} */
		@Override protected void failed(final Throwable cause) {
			super.failed(cause);
			uiPackage.reportErrorToUser("Failed to delete Task List", cause);
		}

		/** {@inheritDoc} */
		@Override
		protected void succeeded(final CTaskList taskList) {
			taskListsModel.removeElement(taskList);
			taskListsView.selectFirstElement();
		}
	}

	/**
	 * Removes Task from google account.<p>
	 * Updates UI if task is successfully deleted, shows error dialog with stack trace otherwise. 
	 */
	class RemoveTaskProcess extends Task<CTask, Integer> {
		
		private CTask task;
		
		/** The constructor. */
		public RemoveTaskProcess(final CTask task) {
            super(uiPackage.getApplication());
            this.task = task;
		}

		/** {@inheritDoc} */
		@Override protected CTask doInBackground() throws Exception {
			service.deleteTask(taskListsView.getSelectedTaskList().getId(), task);
			return task;
		}

		/** {@inheritDoc} */
		@Override protected void failed(final Throwable cause) {
			super.failed(cause);
			uiPackage.reportErrorToUser("Failed to delete Task", cause);
		}

		/** {@inheritDoc} */
		@Override
		protected void succeeded(final CTask task) {
			tasksTableModel.removeRow(task);
		}
	}

	/**
	 * Removes completed tasks from google account.<p>
	 * Updates UI if tasks are successfully deleted, shows error dialog with stack trace otherwise. 
	 */
	class RemoveCompletedTasksProcess extends Task<Void, Integer> {
		
		/** The constructor. */
		public RemoveCompletedTasksProcess() {
            super(uiPackage.getApplication());
		}

		/** {@inheritDoc} */
		@Override protected Void doInBackground() throws Exception {
			service.deleteCompleted(taskListsView.getSelectedTaskList().getId());
			return null;
		}

		/** {@inheritDoc} */
		@Override protected void failed(final Throwable cause) {
			super.failed(cause);
			uiPackage.reportErrorToUser("Failed to delete Completed Tasks", cause);
		}

		/** {@inheritDoc} */
		@Override
		protected void succeeded(final Void voids) {
			tasksTableModel.removeCompletedTasks();
		}
	}

}
