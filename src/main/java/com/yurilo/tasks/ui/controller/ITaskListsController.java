package com.yurilo.tasks.ui.controller;

import javax.swing.ActionMap;

import org.jdesktop.application.Task;

import com.google.inject.ImplementedBy;
import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.ui.event.TaskCreatedEvent;
import com.yurilo.tasks.ui.event.TaskCreatedListener;
import com.yurilo.tasks.ui.event.TaskListCreatedEvent;
import com.yurilo.tasks.ui.event.TaskListCreatedListener;

@ImplementedBy(TaskListsController.class)
public interface ITaskListsController extends TaskListCreatedListener, TaskCreatedListener{

	String ACTION_PRINT = "action.print";
	String ACTION_REFRESH = "action.refresh";
	String ACTION_CLEAN = "action.clean";
	String ACTION_TASK_REMOVE = "action.task.remove";
	String ACTION_TASK_ADD = "action.task.add";
	String ACTION_LIST_ADD = "action.list.add";

	Task refreshTaskLists();

	Task refreshTasks();

	Task editTask(final String listId, final CTask task);

	Task removeTaskList();

	Task removeTask();

	Task removeCompletedTasks();

	
	void taskListCreated(final TaskListCreatedEvent event);

	void taskCreated(final TaskCreatedEvent event);

	void initializeActions();

	ActionMap getActionMap();

	/**
	 * Prints task list action.
	 */
	void printTaskList();

	void addTask();

	void addTaskList();
	
}