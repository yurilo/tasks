package com.yurilo.tasks.ui;

import java.awt.Desktop;
import java.net.URI;

import javax.swing.ActionMap;
import javax.swing.JOptionPane;

import org.jdesktop.application.Action;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.View;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.yurilo.tasks.auth.PreferencesCredentialStore;
import com.yurilo.tasks.service.IGoogleTasksService;
import com.yurilo.tasks.ui.controller.ITaskListsController;
import com.yurilo.tasks.ui.view.AboutDialog;
import com.yurilo.tasks.ui.view.AuthDialog;
import com.yurilo.tasks.ui.view.IMainView;
import com.yurilo.tasks.ui.view.PreferencesDialog;

/**
 * Tasks application base class for GUI's.
 */
@Singleton
public class TasksApplication extends SingleFrameApplication {

	public static final String ACTION_QUIT = "action.quit";

	public static final String ACTION_ABOUT = "action.about";

	public static final String ACTION_PREFERENCES = "action.preferences";

	private IUIPackage uiPackage;
	
	private IGoogleTasksService service;
	private ITaskListsController controller;
	private AboutDialog aboutDialog;
	
	/** {@inheritDoc} */
	@Override protected void startup() {
        // create and configure the Guice injector 
        Injector injector = Guice.createInjector(new AbstractModule() {
            public void configure() {
				binder().bind(SingleFrameApplication.class).to(TasksApplication.class);
				uiPackage = new UIPackage(getContext());
				bind(IUIPackage.class).toInstance(uiPackage);
            }
        });
        uiPackage = injector.getInstance(IUIPackage.class);
        service = injector.getInstance(IGoogleTasksService.class);
        controller = injector.getInstance(ITaskListsController.class);
        aboutDialog = injector.getInstance(AboutDialog.class);
        
		IMainView mainView = injector.getInstance(IMainView.class);
		mainView.init();
		show((View) mainView);
	}

	/** {@inheritDoc} */
	@Override protected void ready() {
		super.ready();
		String userId = PreferencesCredentialStore.PREFS.get(PreferencesCredentialStore.USER_ID, "");
		boolean authorized = service.isAuthorized(userId);
		if (authorized) {
			service.authenticate(userId, null);
		} else {
			// force user to do auth2 authentication
			authenticationWorkflow();
		}
		controller.refreshTaskLists().execute();
	}

	/** {@inheritDoc} */
	@Override protected void shutdown() {
		super.shutdown();
	}
	
	private void authenticationWorkflow() {
		Object[] options = { "Yes", "Cancel" };
		int value = JOptionPane.showOptionDialog(uiPackage.getMainFrame(), "Press ok to open the link in the browser and grant required permissions\n"
                + "or press cancell to close application.\n", "Authorize",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        if (value != JOptionPane.OK_OPTION) {
        	uiPackage.getMainFrame().dispose();
        	System.exit(1);
        }
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(URI.create(service.preAuthenticate()));
			} catch (Exception exception) {
				uiPackage.reportErrorToUser("Some error occured while calling google API.", exception);
			}
		} else {
			uiPackage.reportErrorToUser("Class is not supported on this platform. Please open generated link manually.");
		}
		//TODO: use injector.
		AuthDialog authDialog = new AuthDialog(uiPackage, service);
		uiPackage.showDialog(authDialog);
	}


	/**
	 * Quits application action.
	 */
	@Action(name = ACTION_QUIT) public void quit() {
		uiPackage.getApplication().exit();
	}

	/**
	 * Show about dialog.
	 */
	@Action(name = ACTION_ABOUT) public void about() {
		uiPackage.showDialog(aboutDialog);
	}

	/**
	 * Show preferences
	 */
	@Action(name = ACTION_PREFERENCES) public void preferences() {
		PreferencesDialog dialog = new PreferencesDialog(uiPackage);
		uiPackage.showDialog(dialog);
	}

	/**
	 * Returns cached action map object.
	 * 
	 * @return action map.
	 */
	public ActionMap getActionMap() {
		return controller.getActionMap();
	}
	
}
