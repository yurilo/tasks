package com.yurilo.tasks.ui;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * Implementation of this interface must contain the ApplicationContext of the Swing Application Framework(bsaf) and some helper methods.
 */
public interface IUIPackage {

	/**
	 * Get the main frame.
	 * @return the main frame
	 */
	JFrame getMainFrame();

	/**
	 * Get application instance.
	 * @return application instance.
	 */
	TasksApplication getApplication();

	/**
	 * Injects resources to the specified class.
	 * @param clazz class to be used for resource injecting.
	 * @param component component to be used for resource injecting.
	 */
	@SuppressWarnings("rawtypes") void injectResourceMap(Class clazz, Component component);

	/**
	 * Shows dialog.
	 * @param dialog dialog to show.
	 */
	void showDialog(JDialog dialog);

	/**
	 * Adds ESC action on dialog.
	 * @param dialog dialog to modify.
	 */
	void addEscapeListener(JDialog dialog);

	/**
	 * Tries to adjust specified components to be the same size.
	 * @param components components to adjust.
	 */
	void makeSameSize(JComponent... components);

    /**
     * Report an error through a dialog box.
     * @param errorMsg - the error message.
     */
	void reportErrorToUser(String errorMsg);

    /**
     * Report an error through a dialog box.
     * <p>
     * <b>NOTE:</b> it will be processed asyncronosly in EDT.
     * @param errorMsg - the error message.
     * @param exception - exception that was thrown
     */
	void reportErrorToUser(String errorMsg, Throwable exception);

}