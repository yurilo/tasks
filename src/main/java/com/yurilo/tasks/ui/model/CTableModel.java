package com.yurilo.tasks.ui.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.beanutils.PropertyUtils;

public class CTableModel<T> extends AbstractTableModel implements CITableModel<T> {

	private List<T> list;
	
	private List<String> columnNames;

	private List<String> propertyNames;

	public CTableModel(final List<String> columnNames, final List<String> propertyNames) {
		this(new ArrayList<T>(), columnNames, propertyNames);
	}

	public CTableModel(final List<T> list, final List<String> columnNames, final List<String> propertyNames) {
		super();
		this.list = list;
		this.columnNames = columnNames;
		this.propertyNames = propertyNames;
		fireTableStructureChanged();
	}

	/** {@inheritDoc} */
	@Override 
	public int getRowCount() {
		return list.size();
	}

	/** {@inheritDoc} */
	@Override
	public int getColumnCount() {
		return columnNames.size();
	}

	/** {@inheritDoc} */
	@Override
    public String getColumnName(int column) {
		return columnNames.get(column);
    }

	/** {@inheritDoc} */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		try {
			return PropertyUtils.getNestedProperty(list.get(rowIndex), propertyNames.get(columnIndex));
		} catch (Exception exception) {
			return "N/A";
		}
	}

	/** {@inheritDoc} */
    @Override
    public void setValueAt(Object value, int row, int col) {
		try {
			PropertyUtils.setProperty(list.get(row), propertyNames.get(col), value);
	    	fireTableCellUpdated(row, col);
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
    }
    
	/** {@inheritDoc} */
    @Override
    public void addRow(T value) {
    	int row = getRowCount();
    	if (list.contains(value)) {
    		return;
    	}
    	list.add(value);
    	fireTableRowsInserted(row, row);
    }

	/** {@inheritDoc} */
    @Override
    public void addRows(Collection<T> values) {
    	if (values == null || values.isEmpty()) 
    		return;
    	int rowCount = getRowCount();
    	int counter = 0;
    	for (T t : values) {
        	if (list.contains(t)) {
        		continue;
        	}
        	counter++;
        	list.add(t);
		}
    	if (counter > 0) {
    		fireTableRowsInserted(rowCount, rowCount + counter - 1);
    	}
    }

	/** {@inheritDoc} */
    @Override
    public void setValue(int rowNumber, T value) {
    	list.set(rowNumber, value);
    	fireTableDataChanged();
    }
    
	/** {@inheritDoc} */
    @Override
    public T getValue(int rowNumber) {
    	return list.get(rowNumber);
    }
    
	/** {@inheritDoc} */
    @Override
    public void removeRow(int row) {
        list.remove(row);
        fireTableRowsDeleted(row, row);
    }

	/** {@inheritDoc} */
    @Override
    public void removeRow(T object) {
    	int indexOf = list.indexOf(object);
    	if (indexOf < 0) {
    		return;
    	}
    	list.remove(indexOf);
    	fireTableRowsDeleted(indexOf, indexOf);
    }

	/** {@inheritDoc} */
    @Override
    public void cleanAll() {
    	int rowCount = getRowCount();
    	if (rowCount > 0) {
	    	list.clear();
	        fireTableRowsDeleted(0, rowCount - 1);
    	}
    }

	/** {@inheritDoc} */
    @Override
	public List<T> getRows() {
		return list;
	}

}
