package com.yurilo.tasks.ui.model;

import com.google.inject.ImplementedBy;
import com.yurilo.tasks.domain.CTask;

@ImplementedBy(TasksTableModel.class)
public interface ITaskTableModel extends CITableModel<CTask> {

	void removeCompletedTasks();
	
}
