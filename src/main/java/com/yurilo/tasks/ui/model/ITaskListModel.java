package com.yurilo.tasks.ui.model;

import javax.swing.ListModel;

import com.google.inject.ImplementedBy;
import com.yurilo.tasks.domain.CTaskList;

@ImplementedBy(TaskListModel.class)
public interface ITaskListModel extends ListModel {

	void removeElement(CTaskList taskList);

	void removeAllElements();

	void addElement(CTaskList taskList);

	boolean contains(CTaskList taskList);

}
