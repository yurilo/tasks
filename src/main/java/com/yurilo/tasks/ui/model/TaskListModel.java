package com.yurilo.tasks.ui.model;

import javax.swing.DefaultListModel;

import com.google.inject.Singleton;
import com.yurilo.tasks.domain.CTaskList;

@Singleton
public class TaskListModel extends DefaultListModel implements ITaskListModel {

	@Override public void removeElement(CTaskList taskList) {
		super.removeElement(taskList);
	}

	@Override public void removeAllElements() {
		super.removeAllElements();
	}

	@Override public void addElement(CTaskList taskList) {
		super.addElement(taskList);
	}

	@Override public boolean contains(CTaskList taskList) {
		return super.contains(taskList);
	}

}
