package com.yurilo.tasks.ui.model;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class TaskListsAndTasksModel implements IModel {
	
	private ITaskTableModel tasksTableModel;
	
	private ITaskListModel taskListsModel;

	@Inject public TaskListsAndTasksModel(ITaskTableModel tasksTableModel, ITaskListModel taskListsModel) {
		super();
		this.tasksTableModel = tasksTableModel;
		this.taskListsModel = taskListsModel;
	}

	/** {@inheritDoc} */
	@Override public ITaskTableModel getTasksTableModel() {
		return tasksTableModel;
	}

	/** {@inheritDoc} */
	@Override public void setTasksTableModel(ITaskTableModel tasksTableModel) {
		this.tasksTableModel = tasksTableModel;
	}

	/** {@inheritDoc} */
	@Override public ITaskListModel getTaskListsModel() {
		return taskListsModel;
	}

	/** {@inheritDoc} */
	@Override public void setTaskListsModel(ITaskListModel taskListsModel) {
		this.taskListsModel = taskListsModel;
	}
	
}
