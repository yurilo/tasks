package com.yurilo.tasks.ui.model;

import java.util.Collection;
import java.util.List;

import javax.swing.table.TableModel;

public interface CITableModel<T> extends TableModel {
	void addRow(T value);

	void addRows(Collection<T> values);

	void setValue(int rowNumber, T value);

	T getValue(int rowNumber);

	void removeRow(int row);

	void removeRow(T object);

	void cleanAll();

	List<T> getRows();

}
