package com.yurilo.tasks.ui.model;

import java.util.Arrays;
import java.util.Iterator;

import javax.swing.ImageIcon;

import com.google.inject.Singleton;
import com.yurilo.tasks.domain.CTask;

@Singleton
public class TasksTableModel extends CTableModel<CTask> implements ITaskTableModel {

    public TasksTableModel() {
		super(Arrays.asList("", "Title", "Due date", "Notes"), Arrays.asList("completed", "title", "dueDate", "notes"));
	}

	/*
     * JTable uses this method to determine the default renderer/
     * editor for each cell.  If we didn't implement this method,
     * then the last column would contain text ("true"/"false"),
     * rather than a check box.
     * Boolean — rendered with a check box.
Number — rendered by a right-aligned label.
Double, Float — same as Number, but the object-to-text translation is performed by a NumberFormat instance (using the default number format for the current locale).
Date — rendered by a label, with the object-to-text translation performed by a DateFormat instance (using a short style for the date and time).
ImageIcon, Icon — rendered by a centered label.
Object — rendered by a label that displays the object's string value.
     */
    @Override public Class getColumnClass(int columnIndex) {
    	if (getColumnName(columnIndex).equals("notes")) {
    		return ImageIcon.class;
    	}
    	if (getValueAt(0, columnIndex) != null) {
    		return getValueAt(0, columnIndex).getClass();
    	}
    	return super.getColumnClass(columnIndex);
    }
    
    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    @Override public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        return true;
    }

	/** {@inheritDoc} */
    @Override public void removeCompletedTasks() {
    	for (Iterator<CTask> iterator = getRows().iterator(); iterator.hasNext();) {
			CTask task = (CTask) iterator.next();
			if (task.isCompleted()) {
		    	int indexOf = getRows().indexOf(task);
		    	if (indexOf < 0) {
		    		return;
		    	}
				iterator.remove();
				fireTableRowsDeleted(indexOf, indexOf);
			}
		}
    }
}