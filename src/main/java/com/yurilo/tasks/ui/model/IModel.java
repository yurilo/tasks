package com.yurilo.tasks.ui.model;

import com.google.inject.ImplementedBy;

//TODO: hold Tasks in all lists
@ImplementedBy(TaskListsAndTasksModel.class)
public interface IModel {

	ITaskTableModel getTasksTableModel();

	void setTasksTableModel(ITaskTableModel tasksTableModel);

	ITaskListModel getTaskListsModel();

	void setTaskListsModel(ITaskListModel taskListsModel);

}