package com.yurilo.tasks.ui.event;

public interface LocaleChangeListener {
    public void localeChanged(LocaleChangeEvent event);
}
