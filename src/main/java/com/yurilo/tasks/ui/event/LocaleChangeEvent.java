package com.yurilo.tasks.ui.event;

import java.util.EventObject;
import java.util.Locale;

public class LocaleChangeEvent extends EventObject {

    private Locale locale;

    public LocaleChangeEvent(final Object source) {
        super(source);
    }

    public LocaleChangeEvent(final Object source, final Locale locale) {
        super(source);
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }
}
