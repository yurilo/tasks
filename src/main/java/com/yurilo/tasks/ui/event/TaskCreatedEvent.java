package com.yurilo.tasks.ui.event;

import java.util.EventObject;

import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.domain.CTaskList;

public class TaskCreatedEvent extends EventObject {

	private CTask task;

	private CTaskList taskList;

	public TaskCreatedEvent(final Object source) {
		super(source);
	}

	public TaskCreatedEvent(final Object source, final CTask task, final CTaskList taskList) {
		super(source);
		this.task = task;
		this.taskList = taskList;
	}

	public CTask getTask() {
		return task;
	}

	public CTaskList getTaskList() {
		return taskList;
	}

}
