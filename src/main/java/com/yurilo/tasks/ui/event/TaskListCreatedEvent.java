package com.yurilo.tasks.ui.event;

import java.util.EventObject;

import com.yurilo.tasks.domain.CTask;
import com.yurilo.tasks.domain.CTaskList;

public class TaskListCreatedEvent extends EventObject {

	private CTaskList taskList;
	
	public TaskListCreatedEvent(final Object source) {
		super(source);
	}

	public TaskListCreatedEvent(final Object source, final CTaskList taskList) {
		super(source);
		this.taskList = taskList;
	}

	public CTaskList getTaskList() {
		return taskList;
	}

}
