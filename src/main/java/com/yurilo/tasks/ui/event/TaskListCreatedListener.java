package com.yurilo.tasks.ui.event;

import java.util.EventListener;

public interface TaskListCreatedListener extends EventListener {

	  void taskListCreated(TaskListCreatedEvent event);

}
