package com.yurilo.tasks.ui.event;

import java.util.EventListener;

public interface TaskCreatedListener extends EventListener {

	  void taskCreated(TaskCreatedEvent event);

}
