package com.yurilo.tasks;

import java.io.IOException;

import org.jdesktop.application.Application;

import com.yurilo.tasks.ui.TasksApplication;

/**
 * Main class that launches the application.
 */
public class Main {
	
	public static void main(final String[] args) throws IOException {
		Application.launch(TasksApplication.class, args);
	}
}
