package com.yurilo.tasks.domain;

import java.util.Date;

import com.google.api.services.tasks.model.TaskList;

/**
 * Wrapper class for <tt>TaskList</tt>.<p>
 * @see TaskList
 */
public class CTaskList {

	private String id;

	private String title;

	private String etag;

	private Date lastModifiedDate;

	public CTaskList(final String title) {
		this.title = title;
	}
	
	public CTaskList(final TaskList taskList) {
		this.id = taskList.getId();
		this.etag = taskList.getEtag();
		this.title = taskList.getTitle();
		this.lastModifiedDate = new Date(taskList.getUpdated().getValue());
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getEtag() {
		return etag;
	}

	public void setEtag(final String etag) {
		this.etag = etag;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(final Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CTaskList other = (CTaskList) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override public String toString() {
		return "CTaskList [id=" + id + ", title=" + title + "]";
	}
}
