package com.yurilo.tasks.domain;

import java.util.Date;

import com.google.api.services.tasks.model.Task;

/**
 * Wrapper class for <tt>Task</tt>.<p>
 * @see Task
 */
public class CTask {
	private String id;
	private String title;
	private boolean isCompleted;
	private Date lastModifiedDate;
	private String etag;
	private String notes;
	private Date dueDate;
	public CTask(final String title) {
		super();
		this.title = title;
	}
	public CTask(final Task task) {
		this.id = task.getId();
		this.title = task.getTitle();
		this.etag = task.getEtag();
		this.notes = task.getNotes();
		this.isCompleted = "completed".equalsIgnoreCase(task.getStatus());
		if (task.getUpdated() != null) {
			this.lastModifiedDate = new Date(task.getUpdated().getValue());
		}
		if (task.getDue() != null) {
			this.dueDate = new Date(task.getDue().getValue());
		}
	}
	public String getId() {
		return id;
	}
	public void setId(final String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(final String title) {
		this.title = title;
	}
	public boolean isCompleted() {
		return isCompleted;
	}
	public void setCompleted(final boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(final Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getEtag() {
		return etag;
	}
	public void setEtag(final String etag) {
		this.etag = etag;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(final String notes) {
		this.notes = notes;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(final Date dueDate) {
		this.dueDate = dueDate;
	}
	@Override public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CTask other = (CTask) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override public String toString() {
		return "CTask [title=" + title + ", isCompleted=" + isCompleted + ", notes=" + notes + ", dueDate=" + dueDate + "]";
	}
	
}
