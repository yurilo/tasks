package com.yurilo.tasks.auth;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.prefs.Preferences;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialStore;

/**
 * OAuth 2.0 credential persistence store interface to provide a fully pluggable storage mechanism.
 * <p>
 * Credentials stored using java preferences.
 * <p>
 * this class provides thread safe implementation.
 */
public class PreferencesCredentialStore implements CredentialStore {

	private final Lock lock = new ReentrantLock();

	/** PreferencesCredentialStore preferences */
	public static final Preferences PREFS = Preferences.userNodeForPackage(com.yurilo.tasks.auth.PreferencesCredentialStore.class);
	
	private static final String ACCESS_TOKEN = "access_token";
	private static final String REFRESH_TOKEN = "refresh_token";
	private static final String EXPIRATION_TIME = "expiration_time";
	public static final String USER_ID = "user_id";
	
	/** {@inheritDoc} */
	public void store(final String userId, final Credential credential) {
			PREFS.put(ACCESS_TOKEN, credential.getAccessToken());
			PREFS.put(REFRESH_TOKEN, credential.getRefreshToken());
			PREFS.put(EXPIRATION_TIME, String.valueOf(credential.getExpirationTimeMilliseconds()));
			PREFS.put(USER_ID, userId);
	}

	/** {@inheritDoc} */
	public void delete(final String userId, final Credential credential) {
			PREFS.remove(ACCESS_TOKEN);
			PREFS.remove(REFRESH_TOKEN);
			PREFS.remove(EXPIRATION_TIME);
			PREFS.remove(USER_ID);
	}

	/** {@inheritDoc} */
	public boolean load(final String userId, final Credential credential) {
		lock.lock();
		try {
			String userIdFromPreference = PREFS.get(USER_ID, null);
			if (userIdFromPreference != null) {
				credential.setAccessToken(PREFS.get(ACCESS_TOKEN, null));
				credential.setRefreshToken(PREFS.get(REFRESH_TOKEN, null));
				credential.setExpirationTimeMilliseconds(PREFS.getLong(EXPIRATION_TIME, 0));
			}
			return userIdFromPreference != null;
		} finally {
			lock.unlock();
		}
	}
}
