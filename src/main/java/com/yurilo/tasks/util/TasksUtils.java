package com.yurilo.tasks.util;

import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

/**
 * This class contains the static utility methods.
 */
public final class TasksUtils {

    private static volatile Locale DEFAULT_LOCALE = Locale.US;

    private static Properties appProperties = new Properties();

    static {
    	try {
    		ClassLoader classLoader = TasksUtils.class.getClassLoader();
			appProperties.load(classLoader.getResourceAsStream("application.properties"));
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    }
    
    public static Properties getProperties() {
        return appProperties;
    }

    /*
    public static String getResString(String key) {
        return getResStringDefault(key, "N/A");
    }
    private static String getResStringDefault(String key, String defaultValue) {
        if (key == null) {
            return null;
        }
        String resKey = key.replace(' ', '_');
        resKey = resKey.toLowerCase(DEFAULT_LOCALE);
        String resString = null;
        try {
            resString = resources.getString(resKey);
        } catch (MissingResourceException mre) {
            resString = defaultValue;
        }
        return resString;
    }
     */
    
	public static String insertPeriodically(String text, String insert, int period) {
		StringBuilder sb = new StringBuilder(text);

		int i = 0;
		while ((i = sb.indexOf(" ", i + period)) != -1) {
		    sb.replace(i, i + 1, insert);
		}

		return sb.toString();
	}

    public static String getAdminEmail() {
        return getProperties().getProperty("admin.email");
    }

}
