package com.yurilo.tasks.exceptions;

/**
 * The generic exception class.
 */
public class CSystemException extends RuntimeException {
	
	/** Serial version id. */
	public static final long serialVersionUID = 5000000001L;
	
	/**
	 * Creates a new <code>CSystemException</code> object with the given message.
	 * 
	 * @param message the reason for this <code>CSystemException</code>.
	 */
	public CSystemException(final String message) {
		super(message);
	}

	/**
	 * Creates a new <code>CSystemException</code> object using the given message and cause exception.
	 * 
	 * @param message the reason for this <code>CSystemException</code>.
	 * @param cause the <code>Throwable</code> that caused this <code>CSystemException</code>.
	 */
	public CSystemException(final String message, final Throwable cause) {
		super(message, cause);
	}
}